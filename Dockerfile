FROM alpine:latest
MAINTAINER wangchangming <wangchangming@caishuo.com>

RUN \
  apk --update --upgrade add curl ca-certificates  && \
  rm /var/cache/apk/*

#------------------------------------------------------------------------------
# Environment variables:
#------------------------------------------------------------------------------

ENV GIN_MODE="release"

#------------------------------------------------------------------------------
# Populate root file system:
#------------------------------------------------------------------------------

ADD pkg /

#------------------------------------------------------------------------------
# Expose ports and entrypoint:
#------------------------------------------------------------------------------
EXPOSE 8080

# ENTRYPOINT ["/usr/sbin/privoxy", "--no-daemon", "/etc/privoxy/config"]
ENTRYPOINT ["/go-gitlab-webhooks"]
