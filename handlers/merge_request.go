package handlers

import (
	"log"
	"os"
	"regexp"
	"strconv"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/bluebu/go-gitlab-webhooks/models"
)

// DefaultMergedLabel 默认MergedLabel
const (
	defaultMergedLabel = "KB[stage][35][Merged]"
	defaultGitlabURL   = "https://gitlab.com/api/v3/"
	gitlabTokenEnvName = "GITLAB_TOKEN"
	gitlabURLEnvName   = "GITLAB_API_URL"
)

// checkLabel 判断数组中是否存在某元素
func checkLabel(labels []string, label string) bool {
	for _, item := range labels {
		if label == item {
			return true
		}
	}
	return false
}

// gitlabURLEnvName 从环境变量中读取gitlab api url, http(s)://gitlab.your-domain.com/api/v3/
func gitlabURL() string {
	return os.Getenv(gitlabURLEnvName)
}

// gitlabToken 从环境变量中读取gitlab token, 获取Token: gitlab.com/profile/account
func gitlabToken() string {
	return os.Getenv(gitlabTokenEnvName)
}

// GitlabClient 获得gitlab client
func GitlabClient() *gitlab.Client {
	git := gitlab.NewClient(nil, gitlabToken())
	git.SetBaseURL(gitlabURL())
	log.Println("加载Gitlab:", git.BaseURL())
	return git
}

// getIssueIIDs 根据merge request标题获取issue iid
// 规则: 凡事符合 /#\d/
// 输入: str := "这个merge_request包含#1,#2 ,还有##3, 以及#41#ok, 或许100, #thirteen, #一百八十八"
// 输出: []int{1, 2, 3, 41}
func getIssueIIDs(title string) []string {
	re, _ := regexp.Compile("#([0-9]+)")
	return re.FindAllString(title, -1)
}

// getIssueIDs, 根据merge request标题获取issue iid
// 然后根据iid, 找到对应的issues
func getIssues(client *gitlab.Client, pid int, title string) (issues []*gitlab.Issue) {
	re, _ := regexp.Compile("[0-9]+")
	iids := getIssueIIDs(title)
	log.Println("从Title解析IID: ", iids)
	for _, iid := range iids {
		iidInt, _ := strconv.Atoi(re.FindString(iid))
		opt := &gitlab.ListProjectIssuesOptions{
			IID: iidInt,
		}
		log.Println("更新Issue: ", client.BaseURL())
		remoteIssues, _, err := client.Issues.ListProjectIssues(pid, opt)
		if err != nil {
			log.Println("ListProjectIssues报错:", err)
			continue
		}

		if len(remoteIssues) == 0 {
			log.Println("找不到远程issue, iid:", iid)
			continue
		}
		// 此处认为通过iid查找到的都是单条issue, 多条issue情况太特殊, 以后再解决
		issues = append(issues, remoteIssues[0])
	}
	log.Println("匹配远程Issue次数:", len(issues))
	return issues
}

// updateIssues 更新issue的tag
func updateIssues(client *gitlab.Client, pid int, issues []*gitlab.Issue) {
	for _, issue := range issues {
		remoteLabels := issue.Labels
		var labels []string

		if checkLabel(remoteLabels, defaultMergedLabel) {
			labels = remoteLabels
			log.Println("跳过label:", defaultMergedLabel)
		} else {
			for _, label := range remoteLabels {
				if match, _ := regexp.MatchString("KB\\[stage\\]", label); !match {
					labels = append(labels, label)
				}
			}
			labels = append(labels, defaultMergedLabel)
			log.Println("添加label:", defaultMergedLabel)
		}
		log.Println(labels)

		opt := &gitlab.UpdateIssueOptions{
			Labels: labels,
		}
		_, _, err := client.Issues.UpdateIssue(pid, issue.ID, opt)

		if err != nil {
			log.Println(err)
		}

	}
}

// MergeRequestMerged MergeRequest合并时的Handler
func MergeRequestMerged(client *gitlab.Client, json *models.RequestObj) {
	log.Println("开始处理: MergeRequestMerged")
	pid := json.ObjectAttributes.PID
	log.Println("ProjectId: ", pid)
	if json.ObjectAttributes.TargetBranch != "master" && json.ObjectAttributes.TargetBranch != "production" {
		log.Println("Skipped TargetBranch:", json.ObjectAttributes.TargetBranch)
		return
	}

	issues := getIssues(client, pid, json.ObjectAttributes.Title)
	updateIssues(client, pid, issues)
}
