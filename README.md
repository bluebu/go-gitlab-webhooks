# Gitlab WebHooks

自定义Gitlab WebHooks

![Gitlab WebHooks For Kanban](img/snapshot.png)

- Usage
  1. [Setup](#setup)
  2. [Run](#run)
  3. [Deploy](#deploy)
  4. [Settings Gitlab](#gitlab)_
- Hooks
  1. [MergeRequestMerged](#MergeRequestMerged)

## Usage

gitlab 和 Kanban 之间的webhook

Feature List:

- [x] Docker部署
- [x] 当MergeRequest被merge时, 相关issue会将Kanban状态也更新为Merged
- [x] 非阻塞执行, 更新Issue等操作采用golang的并发模型

### Setup

```bash
go get gitlab.com/bluebu/go-gitlab-webhooks
go install gitlab.com/bluebu/go-gitlab-webhooks
```

### Run

```bash
GITLAB_API_URL=https://gitlab.your-domain.com/api/v3/ GITLAB_TOKEN=xxxxxx go-gitlab-webhooks
```

默认端口: `8080`

Heartbeat URL: http://your-host:8080/health

Hook URL: http://your-host:8080/hooks/gitlab

### Deploy

```bash
docker run -i -t --rm --name go-gitlab-webhooks bluebu/go-gitlab-webhooks
```

### Gitlab

Open in the Gitlab: Project -> Settings -> WebHooks

1. URL:  http://your-host:8080/hooks/gitlab
2. Trigger: only select `Merge Request events`
3. SSL verification: Disable

That's enough!
