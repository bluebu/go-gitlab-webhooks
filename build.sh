GOOS=linux GOARCH=amd64 go build -o pkg/go-gitlab-webhooks
echo "go build: OK"

img="bluebu/go-gitlab-webhooks:latest"
docker build $docker_opts -t $img .
echo "docker build: OK"

docker push $img
echo "docker push: OK"
