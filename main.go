package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/bluebu/go-gitlab-webhooks/handlers"
	"gitlab.com/bluebu/go-gitlab-webhooks/models"
)

// DefaultMergedLabel 默认MergedLabel
const (
	Version = "v1.0.0"
)

func welcome() {
	fmt.Println("go-gitlab-webhooks", "@", Version)
}

func main() {
	welcome()
	client := handlers.GitlabClient()
	log.Println("加载Gitlab成功:", client.BaseURL())

	router := gin.Default()

	router.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"version": Version,
		})
	})

	// Gitlab WebHooks
	router.POST("/hooks/gitlab", func(c *gin.Context) {

		var json models.RequestObj

		if c.BindJSON(&json) == nil {

			objectKind := json.ObjectKind
			action := json.ObjectAttributes.Action
			eventKey := (objectKind + "|" + action)

			switch eventKey {
			case "merge_request|merge":
				go handlers.MergeRequestMerged(client, &json)
				log.Println(gin.H{
					"event":     c.Request.Header.Get("X-Gitlab-Event"),
					"event_key": eventKey,
					"title":     json.ObjectAttributes.Title,
					"action":    action,
					"version":   Version,
				})
			default:
				log.Println("Should Skipped: " + eventKey)
			}

			c.JSON(http.StatusOK, gin.H{})
		} else {
			c.JSON(http.StatusBadRequest, gin.H{})
		}

	})

	http.ListenAndServe(":8080", router)
}
