package models

// RequestObj Binding from JSON
type RequestObj struct {
	ObjectKind       string `form:"object_kind" json:"object_kind" binding:"required"`
	ObjectAttributes struct {
		Action       string `json:"action"`
		PID          int    `json:"source_project_id"`
		Title        string `json:"title"`
		TargetBranch string `json:"target_branch"`
	} `json:"object_attributes"`
}
