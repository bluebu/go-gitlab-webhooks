package handlers

import (
	"reflect"
	"regexp"
	"testing"

	"gitlab.com/bluebu/go-gitlab-webhooks/models"
)

func Test_Gitlab_1(t *testing.T) {
	client := GitlabClient()
	var json models.RequestObj
	json.ObjectKind = "111"
	json.ObjectAttributes.Action = "merge"
	json.ObjectAttributes.PID = 187

	MergeRequestMerged(client, &json)
	t.Log("这是一个错误")
}

// labels中不包含label, 应该返回false
func Test_checkLabel_1(t *testing.T) {
	labels := []string{"feature"}
	label := defaultMergedLabel
	if checkLabel(labels, label) {
		t.Error("labels中不包含label, 应该返回false")
	} else {
		t.Log("OK")
	}
}

// labels中包含label, 应该返回true
func Test_checkLabel_2(t *testing.T) {
	labels := []string{"feature", defaultMergedLabel}
	label := defaultMergedLabel
	if checkLabel(labels, label) {
		t.Log("OK")
	} else {
		t.Error("labels中包含label, 应该返回true")
	}
}

func Test_getIssueIIDs_1(t *testing.T) {
	title := "这个merge_request包含#1,#2 ,还有##3, 以及#41#ok, 或许100, #thirteen, #一百八十八"
	expect := []string{"#1", "#2", "#3", "#41"}
	result := getIssueIIDs(title)

	if reflect.DeepEqual(result, expect) {
		t.Log(result)
	} else {
		t.Log(expect)
		t.Log(result)
		t.Error("正则结果不匹配")
	}
}

func Test_aaa(t *testing.T) {
	label := "KB[stage][35][Merged]"
	if match, _ := regexp.MatchString("KB\\[stage\\]", label); match {
		t.Log("匹配成功", match)
	} else {
		t.Error("匹配失败", match)
	}
}
